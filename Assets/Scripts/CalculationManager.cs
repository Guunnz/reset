﻿using UnityEngine;
using UnityEngine.UI;
public class CalculationManager : MonoBehaviour {

    public int actualNum;
    public int neededNum;
    public GameObject exit;
    public Text aNum;
    public Text nNum;
    private void Update()
    {
        
        aNum.text = "Actual Number: " + actualNum;
        nNum.text = "Needed Number: " + neededNum;
        if (exit == null)
        {
            FindExit();
        }
        else
        { 
            if (actualNum == neededNum)
            {
                exit.tag = "Untagged";
                exit.GetComponent<Animator>().Play("JailUp");
                Debug.Log("LevelCompleted");
            }
            else
            {
                exit.GetComponent<Animator>().Play("JailDown");
                exit.tag = "Object";
                exit.SetActive(true);
            }
        }
    }

    
    void FindExit()
    {

        exit = GameObject.Find("/Stage/Floor/Exit(Clone)");
        if (exit == null)
        {
            exit = GameObject.Find("/Stage/Ceiling/ExitCeiling(Clone)");
        }
    }
}
