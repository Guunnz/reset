﻿using UnityEngine;
using UnityEngine.UI;
public class rewindSlider : MonoBehaviour {

    GameObject player;
	// Use this for initialization
	void Start ()
    {
        player = GameObject.Find("/Player(Clone)");
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        GetComponent<Slider>().maxValue = player.GetComponent<TimeBody>().timePointsMax;

        GetComponent<Slider>().value = player.GetComponent<TimeBody>().actualTimePoints;
    }
}
