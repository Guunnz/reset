﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class devCheats : MonoBehaviour {

	
	// Update is called once per frame
	void Update ()
    {
        if(Input.GetKey(KeyCode.F10) && Input.GetKey(KeyCode.F2))
        {
            if (levelManager.level <= 0)
            {     levelManager.level = 1;
                return;
            }
            SceneManager.LoadScene("LevelEditor");
        }
        if (Input.GetKey(KeyCode.F10) && Input.GetKey(KeyCode.F1))
        {
            levelManager.level-= 2;
            if (levelManager.level < 0)
            {
                levelManager.level = 0;
                return;
            }
            SceneManager.LoadScene("LevelEditor");
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            levelManager.level -= 1;
            if (levelManager.level < 0)
            {
                levelManager.level = 0;
                return;
            }
            SceneManager.LoadScene("LevelEditor");
        }
    }
}
