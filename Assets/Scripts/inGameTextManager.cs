﻿using UnityEngine;
using UnityEngine.UI;
public class inGameTextManager : MonoBehaviour {

    bool once;
    [SerializeField] GameObject _TutorialText;
    public string[] textBox;
    public bool callText = false;

    private void Awake()
    {
        PlayerPrefs.SetInt("whichText", 0); //SOLUCION MOMENTANIA A PROBLEMAS DE TEXTO, LA FUTURA SOLUCION SERÍA APLICAR ESTA FUNCION EN "NEW GAME".
    }                                       //Y EN LOAD GAME NO NECESITARIAMOS LLAMARLA.
    private void Start()
    {
        callText = false;
        once = false;
    }
    private void Update()
    {
        if (callText && !once)
        {
            once = true;
            _TutorialText.GetComponent<Text>().text = textBox[levelManager.level - 1];
            _TutorialText.GetComponent<Animator>().Play("TutorialTextIn");
        }
    }
}