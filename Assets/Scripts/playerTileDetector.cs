﻿using UnityEngine;

public class playerTileDetector : MonoBehaviour
{
    Transform parent;
    string baseTag = "";
    bool baseTagCalled;
    private void Awake()
    { 
        parent = transform.parent;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (gameObject.tag == "UC")
        {
            if(collision.tag != "UC" && collision.tag != "DC" && collision.tag != "RC" && collision.tag != "LC")
            {
                parent.GetComponent<getTileInfo>().uInfo = collision.tag;
            }
        }
        if (gameObject.tag == "DC")
        {
            if (collision.tag != "UC" && collision.tag != "DC" && collision.tag != "RC" && collision.tag != "LC")
            {
                parent.GetComponent<getTileInfo>().dInfo = collision.tag;
            }
        }
        if (gameObject.tag == "RC")
        {
            if (collision.tag != "UC" && collision.tag != "DC" && collision.tag != "RC" && collision.tag != "LC")
            {
               // Debug.Log(collision.gameObject.GetComponent<getTileInfo>().objectType);
                parent.GetComponent<getTileInfo>().rInfo = collision.tag;
            }
        }
        if (gameObject.tag == "LC")
        {
            if (collision.tag != "UC" && collision.tag != "DC" && collision.tag != "RC" && collision.tag != "LC")
            {
                parent.GetComponent<getTileInfo>().lInfo = collision.tag;
            }
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (gameObject.tag == "UC")
        {
            if (collision.tag != "UC" && collision.tag != "DC" && collision.tag != "RC" && collision.tag != "LC")
                parent.GetComponent<getTileInfo>().uInfo = baseTag;
        }
        if (gameObject.tag == "DC")
        {
            if (collision.tag != "UC" && collision.tag != "DC" && collision.tag != "RC" && collision.tag != "LC")
                parent.GetComponent<getTileInfo>().dInfo = baseTag;
        }
        if (gameObject.tag == "RC")
        {
            if (collision.tag != "UC" && collision.tag != "DC" && collision.tag != "RC" && collision.tag != "LC")
                parent.GetComponent<getTileInfo>().rInfo = baseTag;
        }
        if (gameObject.tag == "LC")
        {
            if (collision.tag != "UC" && collision.tag != "DC" && collision.tag != "RC" && collision.tag != "LC")
                parent.GetComponent<getTileInfo>().lInfo = baseTag;
        }
    }

}
