﻿using UnityEngine;
using UnityEngine.UI;
public class playerController : getTileInfo
{
    [Tooltip("Multiplica el valor de tiles por el cual el personaje se mueve en X")]
    [SerializeField] float tilesToMoveX = 1;
    [Tooltip("Multiplica el valor de tiles por el cual el personaje se mueve en Y")]
    [SerializeField] float tilesToMoveY = 1;
    [HideInInspector] float delay;
    Rigidbody2D rb;
    [HideInInspector] public Vector2 lastPlayerPos;
    public bool onBrokenGround;
    [HideInInspector] public bool onGravityTile;
    public bool interact;
    public GameObject eyes;
    [HideInInspector] public float lookingX;
    [HideInInspector] public float lookingY;
    public Text tutorialText;
    bool died;
    public bool playerHasBox;
    bool changedGravity;
    bool gravityCalled;


    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        Debug.Log(interact);
        if (rInfo == "Interactable" || uInfo == "Interactable" || dInfo == "Interactable" || rInfo == "Interactable")
        {
            interact = true;
        }
        if (rInfo != "Interactable" && uInfo != "Interactable" && dInfo != "Interactable" && rInfo != "Interactable")
        {
            interact = false;
        }
        if (Input.GetButtonDown("Gravity") && onGravityTile && !gravityCalled)
        {
            gravityCalled = true;
            delay = 1.5f;
            changedGravity = !changedGravity;
        }
        if (delay <= .2f)
        {
            gravityCalled = false;
        }
    }
    private void FixedUpdate()
    {
        delay -= Time.deltaTime;
        if (!changedGravity)
        {
            float x = Input.GetAxisRaw("Horizontal");
            float y = Input.GetAxisRaw("Vertical");
            Move(x, y);
        }
        else
        {
            float x = Input.GetAxisRaw("Horizontal") * -1;
            float y = Input.GetAxisRaw("Vertical");
            Move(x, y);
        }
       
        
    }

    public void Move(float moveX, float moveY)
    {
        if (!died)
        {
            if (delay < 0)
            {
                if (moveX > 0 && rInfo != "Object" || moveX < 0 && lInfo != "Object")
                {

                    delay = .2f;
                    transform.position = new Vector2((int)(transform.position.x + moveX * tilesToMoveX), transform.position.y);

                }
                else if (moveY < 0 && dInfo != "Object" || moveY > 0 && uInfo != "Object")
                {
                    delay = .2f;
                    transform.position = new Vector2(transform.position.x, (int)transform.position.y + moveY * tilesToMoveY);
                }
                lastPlayerPos = transform.position;
            }
            if (moveX > 0)
            {
                lookingY = 0;
                lookingX = moveX;
                if (!playerHasBox)
                {
                    eyes.transform.rotation = Quaternion.Euler(0, 0, -90f);
                }
            }
            if (moveX < 0)
            {
                lookingY = 0;
                lookingX = moveX;
                if (!playerHasBox)
                {
                    eyes.transform.rotation = Quaternion.Euler(0, 0, 90f);
                }
            }
            if (moveY < 0)
            {
                lookingX = 0;
                lookingY = moveY;
                if (!playerHasBox)
                {
                    eyes.transform.rotation = Quaternion.Euler(0, 0, 180f);
                }
            }
            if (moveY > 0)
            {
                lookingX = 0;
                lookingY = moveY;
                if (!playerHasBox)
                {
                    eyes.transform.rotation = Quaternion.Euler(0, 0, 0);
                }
            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "DeathTile")
        {
            died = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "DeathTile")
        {
            died = false;
        }
    }
}