﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CPP : MonoBehaviour
{

    GameObject CM;


    private void Awake()
    {

        CM = GameObject.FindGameObjectWithTag("Calculator");
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<getTileInfo>().objectType == "Solid")
        {
            CM.GetComponent<CalculationManager>().actualNum += 2;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<getTileInfo>().objectType == "Solid")
        {
            CM.GetComponent<CalculationManager>().actualNum -= 2;
        }
    }
}
