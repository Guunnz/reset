﻿using UnityEngine;

public class objBehaviour : getTileInfo
{
    Rigidbody2D rb;
    GameObject player;
    Vector2 pcPos;
    Vector2 myPos;
    float pcLookX;
    float pcLookY;
    public bool onWall;
    public bool interact;
    bool thisBoxIsPicked;

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        rb = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        Debug.Log(onWall);
        pcPos = player.GetComponent<playerController>().lastPlayerPos;
        pcLookX = player.GetComponent<playerController>().lookingX;
        pcLookY = player.GetComponent<playerController>().lookingY;
        myPos = transform.position;
        if (Input.GetButton("Interact") && !player.GetComponent<playerController>().onBrokenGround && !onWall)
        {
            if (pcPos.y == myPos.y + 1 && pcLookY < 0 && pcPos.x == myPos.x || pcPos.y == myPos.y && pcPos.x == myPos.x +1 && pcLookX < 0 || pcPos.x == myPos.x && pcPos.y == myPos.y -1 && pcLookY > 0 || pcPos.y == myPos.y && pcPos.x == myPos.x - 1 && pcLookX > 0)
            {
                if (!player.GetComponent<playerController>().playerHasBox)
                {
                    player.GetComponent<playerController>().playerHasBox = true;
                    thisBoxIsPicked = true;
                    interact = true;
                }
            }
        }
        else if(thisBoxIsPicked)
        {
            player.GetComponent<playerController>().playerHasBox = false;
            thisBoxIsPicked = false;
            interact = false;
        }
        else
        {
            thisBoxIsPicked = false;
            interact = false;
        }
            if (pcPos.y == myPos.y + 1 && pcPos.x == myPos.x + 1|| pcPos.y == myPos.y +1 && pcPos.x == myPos.x - 1 || pcPos.x == myPos.x -1 && pcPos.y == myPos.y - 1 || pcPos.y == myPos.y-1 && pcPos.x == myPos.x + 1)
            {
                interact = false;
            }
        if (interact && thisBoxIsPicked)
        {
            player.GetComponent<playerController>().playerHasBox = true;
            if (lInfo == "Player" && rInfo != "Object")
            {
                gameObject.tag = "Interactable";
            }
            else if (rInfo == "Player" && lInfo != "Object")
            {
                gameObject.tag = "Interactable";
            }
            else if (uInfo == "Player" && dInfo != "Object")
            {
                gameObject.tag = "Interactable";
            }
            else if (dInfo == "Player" && uInfo != "Object")
            {
                gameObject.tag = "Interactable";
            }
            else
            {
                player.GetComponent<playerController>().playerHasBox = false;
                gameObject.tag = "Object";
            }
            if (pcPos.y == myPos.y && interact && uInfo == "Player" && dInfo != "Object")
            {
                rb.MovePosition(new Vector2(myPos.x, myPos.y - 1));
            }
            if (pcPos.y == myPos.y && interact && dInfo == "Player" && uInfo != "Object")
            {
                rb.MovePosition(new Vector2(myPos.x, myPos.y + 1));
            }
            if (pcPos.x == myPos.x && interact && rInfo == "Player" && lInfo != "Object")
            {
                rb.MovePosition(new Vector2(myPos.x - 1, myPos.y));
            }
            if (pcPos.x == myPos.x && interact && lInfo == "Player" && rInfo != "Object")
            {
                rb.MovePosition(new Vector2(myPos.x + 1, myPos.y));
            }
            player.GetComponent<playerController>().playerHasBox = true;
            if (player.GetComponent<playerController>().onBrokenGround == false && !onWall)
            {
                if (pcPos.x == myPos.x + 2)
                {
                    rb.MovePosition(new Vector2(myPos.x + 1, myPos.y));
                }
                if (pcPos.x == myPos.x - 2)
                {
                    rb.MovePosition(new Vector2(myPos.x - 1, myPos.y));
                }
                if (pcPos.y == myPos.y - 2)
                {
                    rb.MovePosition(new Vector2(myPos.x, myPos.y - 1));
                }
                if (pcPos.y == myPos.y + 2)
                {
                    rb.MovePosition(new Vector2(myPos.x, myPos.y + 1));
                }
            }
            else
            {
                if (pcPos.x == myPos.x + 2)
                {
                    interact = false;
                    return;
                }
                if (pcPos.x == myPos.x - 2)
                {
                    interact = false;
                    return;
                }
                if (pcPos.y == myPos.y - 2)
                {
                    interact = false;
                    return;
                }
                if (pcPos.y == myPos.y + 2)
                {
                    interact = false;
                    return;
                }
            }
             
            }
        else
        {
            gameObject.tag = "Object";
        }
    }
}