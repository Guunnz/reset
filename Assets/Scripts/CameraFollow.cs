﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{

    public float xMargin = 1f; // Distance in the x axis the player can move before the camera follows.
    public float yMargin = 1f; // Distance in the y axis the player can move before the camera follows.
    public float xSmooth = 8f; // How smoothly the camera catches up with it's target movement in the x axis.
    public float ySmooth = 8f; // How smoothly the camera catches up with it's target movement in the y axis.
    public Vector2 maxXAndY; // The maximum x and y coordinates the camera can have.
    public Vector2 minXAndY; // The minimum x and y coordinates the camera can have.
    public float offsetX;
    public float offsetY;
    static public float ymaxmodif;
    static public float ymodif;
    public bool yFix;
    static public bool onGravity = false;
    bool calling;

    private Transform player; // Reference to the player's transform.

    private void Start()
    {
        onGravity = false;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        ymodif = 0;
    }


    private bool CheckXMargin()
    {
        // Returns true if the distance between the camera and the player in the x axis is greater than the x margin.
        return Mathf.Abs(transform.position.x - player.position.x) > xMargin;
    }


    private bool CheckYMargin()
    {
        // Returns true if the distance between the camera and the player in the y axis is greater than the y margin.
        return Mathf.Abs(transform.position.y - player.position.y) > yMargin;
    }

    private void Update()
    {
        TrackPlayerY();
        if (calling && onGravity && transform.localPosition.z < 0.37f)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z + 21 * Time.deltaTime);
        }else if (calling && !onGravity && transform.localPosition.z > -19.404f)
        {
            transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, transform.localPosition.z - 21 * Time.deltaTime);
        }
        if (Input.GetButtonDown("Gravity") && player.GetComponent<playerController>().onGravityTile && !player.GetComponent<playerController>().interact)
        {
            if(!onGravity && !calling)
           {
                
                transform.parent.GetComponent<Animator>().enabled = true;
                calling = true;
                transform.parent.GetComponent<Animator>().Play("GravityOn");
                onGravity = true;
                Invoke("callingFalse", 2f);
            }
            else if (!calling)
            {
                transform.parent.GetComponent<Animator>().enabled = true;
                calling = true;
                transform.parent.GetComponent<Animator>().Play("GravityOff");
                onGravity = false;
                Invoke("callingFalse", 2f);
            }
        }
    }
    void callingFalse()
    {
        transform.parent.GetComponent<Animator>().enabled = false;
        transform.parent.GetComponent<Animator>().StopPlayback();
        calling = false;
    }
    private void LateUpdate()
    {
        TrackPlayerY();
    }

    private void FixedUpdate()
    {
        TrackPlayerY();
        if(!calling)
        {
            TrackPlayerX();
        }

    }
    private void TrackPlayerX()
    {
        // By default the target x and y coordinates of the camera are it's current x and y coordinates.
        float targetX = transform.position.x;

        // If the player has moved beyond the x margin...
        if (CheckXMargin())
            // ... the target x coordinate should be a Lerp between the camera's current x position and the player's current x position.
            targetX = Mathf.Lerp(transform.position.x, player.position.x, xSmooth * Time.deltaTime);

        // The target x and y coordinates should not be larger than the maximum or smaller than the minimum.
        targetX = Mathf.Clamp(targetX, minXAndY.x, maxXAndY.x);

        // Set the camera's position to the target position with the same z component.
        transform.position = new Vector3(targetX + offsetX, transform.position.y, transform.position.z);
    }

private void TrackPlayerY()
    {
        // By default the target x and y coordinates of the camera are it's current x and y coordinates.
        float targetY = transform.position.y;


        // If the player has moved beyond the y margin...
        if (CheckYMargin())
            // ... the target y coordinate should be a Lerp between the camera's current y position and the player's current y position.
            targetY = Mathf.Lerp(transform.position.y, player.position.y, ySmooth * Time.deltaTime);

        // The target x and y coordinates should not be larger than the maximum or smaller than the minimum.
        targetY = Mathf.Clamp(targetY, minXAndY.y + ymodif, maxXAndY.y + ymaxmodif);

        // Set the camera's position to the target position with the same z component.
        if (!yFix)
        {
            transform.position = new Vector3(transform.position.x, targetY + offsetY, transform.position.z);
        }
        if (yFix)
        {
            transform.position = new Vector3(transform.position.x, targetY, transform.position.z);
        }
    }
}
