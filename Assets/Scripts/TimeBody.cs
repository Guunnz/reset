﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeBody : MonoBehaviour {

    public bool isRewinding = false;
    GameObject gameMaster;
    List<Vector2> pointsInTime;
    [HideInInspector] public int timePointsMax;
    [HideInInspector] public int actualTimePoints;
    private void Start()
    {
        gameMaster = GameObject.Find("/_GM");
        pointsInTime = new List<Vector2>();
    }

    private void FixedUpdate()
    {
        actualTimePoints = pointsInTime.Count;
        if (isRewinding)
            Rewind();
        else
            Record();
    }
    void Record()
    {
        if (pointsInTime.Count > Mathf.Round(180f / Time.fixedDeltaTime))
        {
            pointsInTime.RemoveAt(pointsInTime.Count - 1);
        }
        pointsInTime.Insert(0, transform.position);
        timePointsMax = pointsInTime.Count;
    }

    void Rewind()
    {
        if (pointsInTime.Count > 0)
        {
            gameMaster.GetComponent<UIToggles>().timeRewindEffect.SetActive(true);
            transform.position = pointsInTime[0];
            pointsInTime.RemoveAt(0);
        }
        else
        {
            StopRewind();
        }
    }

    // Update is called once per frame
    void Update ()
    {
        if (Input.GetButton("Rewind"))
        {
            StartRewind();
        }
        if (Input.GetButtonUp("Rewind"))
        {
            StopRewind();
        }
    }
    public void StartRewind()
    {
        isRewinding = true;
    }
    public void StopRewind()
    {
        gameMaster.GetComponent<UIToggles>().timeRewindEffect.SetActive(false);
        isRewinding = false;
    }
}
