﻿using UnityEngine;
using UnityEngine.SceneManagement;
public class EndLevel : MonoBehaviour {


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            loadScene();
        }
    }
    void loadScene()
    {
        SceneManager.LoadScene("LevelEditor");
    }
}
