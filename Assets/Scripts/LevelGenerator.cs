﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;
public class LevelGenerator : MonoBehaviour
{
    
    Texture2D[] map;
    public string[] riddles;
    public ColorToPrefab[] colorFloor;
    public ColorToPrefab[] colorPerma;
    public ColorToPrefab[] colorCeilings;
    public Transform ceiling;
    public Transform floor;
    GameObject calculator;
    int maxLevel = 1;
    int mapNum;
    int tileNum;
    public Text lvlTxt;
    public Text maxlvlTxt;
    public Text riddle;
    string mapSolution;
    bool levelChoosing = true;
    
    // Use this for initialization
    private void Awake()
    {
       map = Resources.LoadAll("Maps", typeof(Texture2D)).Cast<Texture2D>().ToArray();
    }
    void Start()
    {
        levelManager.level++;
        print(levelManager.level);
        calculator = GameObject.Find("/Calculator");
        mapNum = Random.Range(0, map.Length);
        maxLevel = riddles.Length;
        while (levelChoosing)
        {
            if (levelManager.level > maxLevel)
            {
                levelManager.level = 1;
            }
            if (map[mapNum].name.Contains("LV" + levelManager.level.ToString() + "-"))
            {
                riddle.text = riddles[levelManager.level - 1];
                mapSolution = map[mapNum].name.Substring((map[mapNum].name.Length - 2),2);
                calculator.GetComponent<CalculationManager>().neededNum = int.Parse(mapSolution);
                levelChoosing = false;
                GenerateLevel();
            }
            else
            {
                mapNum = Random.Range(0, map.Length);
            }
        }
   
        lvlTxt.text = "Current level: " + levelManager.level.ToString();
        maxlvlTxt.text = "Max. level: " + maxLevel.ToString();
    }
    void GenerateLevel()
    {
        if (map[mapNum].name.Contains("CC"))
        {
            for (int x = map[mapNum].width / 2; x < map[mapNum].width; x++)
            {
                for (int y = 0; y < map[mapNum].height; y++)
                {
                    GenerateCeiling(x, y);
                }
            }
            for (int x = 0; x < map[mapNum].width / 2; x++)
            {
                for (int y = 0; y < map[mapNum].height; y++)
                {
                    GenerateFloor(x, y);
                }
            }
        }
        else
        {
            for (int x = 0; x < map[mapNum].width; x++)
            {
                for (int y = 0; y < map[mapNum].height; y++)
                {
                    GenerateFloor(x, y);
                }
            }
        }
    }
    void GenerateCeiling(int x, int y)
    {
        Color pixelColor = map[mapNum].GetPixel(x,y);

        if (pixelColor.a == 0)
        {
            return;
        }
         Vector2 position = new Vector2(x - map[mapNum].width / 2, y);

        foreach (ColorToPrefab __Object in colorCeilings)
        {
            if (__Object.color.Equals(pixelColor))
            {
                Instantiate(__Object.prefab, position, Quaternion.identity,ceiling);
            }
        }
    }
    void GenerateFloor(int x, int y)
    {
       Color pixelColor = map[mapNum].GetPixel(x, y);

        if (pixelColor.a == 0)
        {
            return;
        }

        Vector2 position = new Vector2(x, y);

        foreach (ColorToPrefab _Object in colorFloor)
        {
            if (_Object.color.Equals(pixelColor))
            {
                Instantiate(_Object.prefab, position, Quaternion.identity,floor);
            }
        }
        foreach (ColorToPrefab @object in colorPerma)
        {
            if (@object.color.Equals(pixelColor))
            {
                Instantiate(@object.prefab, position, Quaternion.identity);
            }
        }
    }
}