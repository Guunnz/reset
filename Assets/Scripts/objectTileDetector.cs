﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class objectTileDetector : MonoBehaviour
{
    Transform parent;
    string baseTag = "";
    bool updateColliders;
    private void Awake()
    {
        parent = transform.parent;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (gameObject.tag == "UC")
        {
            if (collision.tag != "UC" && collision.tag != "DC" && collision.tag != "RC" && collision.tag != "LC")
            {
                parent.GetComponent<getTileInfo>().uInfo = collision.tag;
            }
        }
        if (gameObject.tag == "DC")
        {
            if (collision.tag != "UC" && collision.tag != "DC" && collision.tag != "RC" && collision.tag != "LC")
            {
                parent.GetComponent<getTileInfo>().dInfo = collision.tag;
            }
        }
        if (gameObject.tag == "RC")
        {
            if (collision.tag != "UC" && collision.tag != "DC" && collision.tag != "RC" && collision.tag != "LC")
            {
                parent.GetComponent<getTileInfo>().rInfo = collision.tag;
            }
        }
        if (gameObject.tag == "LC")
        {
            if (collision.tag != "UC" && collision.tag != "DC" && collision.tag != "RC" && collision.tag != "LC")
            {
                parent.GetComponent<getTileInfo>().lInfo = collision.tag;
            }
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (updateColliders)
        {
            updateColliders = false;
            if (gameObject.tag == "UC")
            {
                if (collision.tag != "UC" && collision.tag != "DC" && collision.tag != "RC" && collision.tag != "LC")
                {
                    parent.GetComponent<getTileInfo>().uInfo = collision.tag;
                }
            }
            if (gameObject.tag == "DC")
            {
                if (collision.tag != "UC" && collision.tag != "DC" && collision.tag != "RC" && collision.tag != "LC")
                {
                    parent.GetComponent<getTileInfo>().dInfo = collision.tag;
                }
            }
            if (gameObject.tag == "RC")
            {
                if (collision.tag != "UC" && collision.tag != "DC" && collision.tag != "RC" && collision.tag != "LC")
                {
                    parent.GetComponent<getTileInfo>().rInfo = collision.tag;
                }
            }
            if (gameObject.tag == "LC")
            {
                if (collision.tag != "UC" && collision.tag != "DC" && collision.tag != "RC" && collision.tag != "LC")
                {
                    parent.GetComponent<getTileInfo>().lInfo = collision.tag;
                }
            }
        }
    }
        private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag != "UC" && collision.tag != "DC" && collision.tag != "RC" && collision.tag != "LC")
        {
            Invoke("setBase", .1f);
        }
    }

    void setBase()
    {
        if (gameObject.tag == "UC")
        {
            updateColliders = true;
                parent.GetComponent<getTileInfo>().uInfo = baseTag;
        }
        if (gameObject.tag == "DC")
        {
            updateColliders = true;
            parent.GetComponent<getTileInfo>().dInfo = baseTag;
        }
        if (gameObject.tag == "RC")
        {
            updateColliders = true;
            parent.GetComponent<getTileInfo>().rInfo = baseTag;
        }
        if (gameObject.tag == "LC")
        {
            updateColliders = true;
            parent.GetComponent<getTileInfo>().lInfo = baseTag;
        }
    }
}

