﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InPlaceDetectorRock : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Object")
        {
            if (collision.GetComponent<wallBehaviour>() != null)
            {
                brokenGroundTrue();
                if (CameraFollow.onGravity)
                {
                    transform.parent.position = new Vector3(transform.parent.position.x, transform.parent.position.y, 2.5f);
                }
                else
                {
                    transform.parent.position = new Vector3(transform.parent.position.x, transform.parent.position.y, -2.5f);
                }
            }
            else
            {
                brokenGroundTrue();
            }
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Object")
        {
            if (collision.GetComponent<wallBehaviour>() != null)
            {
                brokenGroundTrue();
                if (CameraFollow.onGravity)
                {
                    transform.parent.position = new Vector3(transform.parent.position.x, transform.parent.position.y, 2.5f);
                }
                else
                {
                    transform.parent.position = new Vector3(transform.parent.position.x, transform.parent.position.y, -2.5f);
                }
            }
            else
            {
                brokenGroundTrue();
            }
        }
      //  else
     //   {
      //      brokenGroundFalse();
        //}
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Object")
        {
            if (collision.GetComponent<wallBehaviour>() != null)
            {
                transform.parent.position = new Vector3(transform.parent.position.x, transform.parent.position.y, 0.45f);
                Invoke("brokenGroundFalse", 0.2f);
            }
            else
            {
                Invoke("brokenGroundFalse", 0.2f);
            }
        }
    }
    void brokenGroundFalse()
    {
        transform.parent.GetComponent<objBehaviour>().onWall = false;
    }
    void brokenGroundTrue()
    {
        transform.parent.GetComponent<objBehaviour>().onWall = true;
    }
}
