﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class inPlaceDetector : MonoBehaviour {

    GameObject gameMaster;
    private void Awake()
    {
        gameMaster = GameObject.Find("/_GM");
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Object")
        {
            Invoke("brokenGroundTrue",0.1f);
        }
        if (collision.tag == "TextTile" && !gameMaster.GetComponent<inGameTextManager>().callText)
        {
            gameMaster.GetComponent<inGameTextManager>().callText = true;
        }
        if (collision.tag == "DeathTile" && !gameMaster.GetComponent<inGameTextManager>().callText)
        {
            gameMaster.GetComponent<inGameTextManager>().callText = true;
        }
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "GravityTile")
        {
            transform.parent.GetComponent<playerController>().onGravityTile = true;
        }
        else
        {
            transform.parent.GetComponent<playerController>().onGravityTile = false;
        }
        if (collision.tag == "Object")
        {
            transform.parent.GetComponent<playerController>().onBrokenGround = true;
        }
        else
        {
            transform.parent.GetComponent<playerController>().onBrokenGround = false;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Object")
        {
            Invoke("brokenGroundFalse", 0.1f);
        }
        if (collision.tag == "GravityTile")
        {
            transform.parent.GetComponent<playerController>().onGravityTile = false;
        }
    }
    void brokenGroundFalse()
    {
        transform.parent.GetComponent<playerController>().onBrokenGround = false;
    }
    void brokenGroundTrue()
    {
        transform.parent.GetComponent<playerController>().onBrokenGround = true;
    }
}
